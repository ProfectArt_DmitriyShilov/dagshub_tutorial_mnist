# DAGsHub MNIST Tutorial
This repo serves as an example of how to use DVC and DAGsHub. 

Please follow the [tutorial instructions](https://dagshub.com/docs/tutorial/overview/) for more information.